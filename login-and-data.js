const path = require('path');
const fs = require('fs/promises');
const fsCallback = require('fs');

// Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
// Wait for 2 seconds and starts deleting them one after another.
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

function writeFile(fileName, data) {
    return fs.writeFile(
        path.join(__dirname, fileName),
        data
    );

}

function deleteFile(fileName) {
    return fs.unlink(
        path.join(__dirname, fileName)
    )
}

function readFile(fileName) {
    return fs.readFile(
        path.join(__dirname, fileName)
    )
        .then((data) => {
            return data.toString();
        })

}

function problem1() {
    let files = ["file1", "file2"];

    let fileCreate = files.map((file) => {
        return writeFile(file, "this is file");
    })

    Promise.all(fileCreate).then(() => {
        console.log("files created...");
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                resolve()
            }, 1000 * 2)
        })
    })
        .then(() => {
            let fileDelete = files.map((file) => {
                return deleteFile(file);
            })
            return Promise.all(fileDelete)
        })
        .then(() => {
            console.log("file deleted...");
        })
        .catch((error) => {
            console.log(error);
        })

}


// Q2. Create a new file with lipsum data (you can google and get this). 
// Do File Read and write data to another file
// Delete the original file 

function problem2() {
    //     A. using promises chaining
    fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1%22")
        .then((data) => {
            if (data.ok) {
                return data.json();
            }
        })
        .then((data) => {
            return writeFile("lipsum.txt", data);
        })
        .then(() => {
            return readFile("lipsum.txt");
        })
        .then((fileData) => {
            return fileData;
        })
        .then((fileData) => {
            return writeFile("promise_Lipsum.txt", fileData)
        })
        .then(() => {
            return deleteFile("lipsum.txt");
        })
        .catch((error) => {
            console.log(error);
        })


    // B. using callbacks
    fetch("https://baconipsum.com/api/?type=all-meat&sentences=100&start-with-lorem=1%22")
        .then((data) => {
            if (data.ok) {
                return data.json();
            }
        })
        .then((data) => {

            fsCallback.writeFile(path.join(__dirname, "lipsum2.txt"), `${data}`, ((err) => {
                if (err) {
                    throw new Error(err);
                }
                else {
                    console.log("lipsum2 created..");

                    fsCallback.readFile(path.join(__dirname, "lipsum2.txt"), ((err, fileData) => {
                        if (err) {
                            throw new Error(err);
                        } else {
                            console.log("reading done lipsum2");

                            fsCallback.writeFile(path.join(__dirname, "fs_Lipsum.txt"), fileData.toString(), ((err) => {
                                if (err) {
                                    throw new Error(err);
                                } else {
                                    console.log("lipsum2 copy into fsLipsum");

                                    fsCallback.unlink(path.join(__dirname, "lipsum2.txt"), ((err) => {
                                        if (err) {
                                            throw new Error(err);
                                        } else {
                                            console.log("lipsum2 deleted..");
                                        }
                                    })
                                    )
                                }
                            })
                            )
                        }
                    })
                    )
                }
            })
            )

        })
}


//-----problem 3-------
// A. login with value 3 and call getData once login is successful

// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    fs.appendFile(
        path.join(__dirname, 'log.txt'),
        `${user}--${activity}--${new Date()}\n`
    )
        .then(() => {
            console.log('log saved..');
        })
        .catch((err) => {
            console.log(err);
        })
}

function problem3(user, id) {
    login(user, id)
        .then((user) => {
            logData(user, "Login Success")
            getData()
                .then((data) => {
                    let userDetails = data.filter((user) => {
                        return user.id === id
                    })
                    if (userDetails.length > 0) {
                        logData(user, 'GetData Success')
                    } else {
                        logData(user, ' GetData Failure')
                    }
                })
        })
        .catch((error) => {
            console.log("login failed: ", error.message);
            getData().then((data) => {
                let userDetails = data.filter((info) => {
                    return info.id == id
                })
                if (userDetails.length > 0) {
                    logData(user, 'GetData Success')
                } else {
                    logData(user, ' GetData Failure')
                }
            })
        })
}
